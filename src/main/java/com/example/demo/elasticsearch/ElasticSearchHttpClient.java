package com.example.demo.elasticsearch;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class ElasticSearchHttpClient {

	private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchHttpClient.class);
	private final String baseUrl;
	//private final ElasticSearchURLs searchURLs;
	
	
	/*@Autowired
	RestTemplate restTemplate;*/
	
	
	public ElasticSearchHttpClient(final String baseUrl) {

		this.baseUrl = baseUrl;
		System.out.println("elasticsearch url:================"+ baseUrl);
		//this.searchURLs = new ElasticSearchURLs();
	}
	
	private  static HttpHeaders getHttpHeaders(){

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}
	
	 
	/*
	public  List<Source> getResponse(final ElasticSearch es,final Integer offset,final Integer limit,final String tag) {
		 List<Source> users = new ArrayList<Source>();
		final HttpEntity<ElasticSearch> reqEntity = new HttpEntity<ElasticSearch>(es,getHttpHeaders());
		try {
			LOG.info("Json query: ==========> "+new ObjectMapper().writeValueAsString(es));
			final ResponseEntity<SearchResponse> resEntity = restTemplate.exchange(searchURLs.getSearchUrl(offset,limit),
					HttpMethod.POST,reqEntity,SearchResponse.class);	
			final List<Hit> hitList = resEntity.getBody().getHits().getHits();
			for (Hit hit : hitList) {
				users.add(hit.getSource());
			}
		} catch (Exception e) {
			LOG.error("ElasticSearch :================Error While  Search ===========["+tag+"]==========" ,e);
		}
		return users;
	}
	
	public  List<Source> getResponse1(final ElasticSearch es,final String tag) {
		 List<Source> users = new ArrayList<Source>();
		final HttpEntity<ElasticSearch> reqEntity = new HttpEntity<ElasticSearch>(es,getHttpHeaders());
		try {
			LOG.info("Json: ==========> "+new ObjectMapper().writeValueAsString(es));
			final ResponseEntity<SearchResponse> resEntity = restTemplate.exchange(searchURLs.getUserProperityUrl(),
					HttpMethod.POST,reqEntity,SearchResponse.class);	
			final List<Hit> hitList = resEntity.getBody().getHits().getHits();
			for (Hit hit : hitList) {
				users.add(hit.getSource());
			}
		
		} catch (Exception e) {
			LOG.error("ElasticSearch :================Error While  Search ===========["+tag+"]==========" ,e);
		}
		return users;
	}
   
	public Optional<Source> getUserDocument(Long userId) {
		
		final HttpEntity<String> reqEntity = new HttpEntity<String>(getHttpHeaders());
		try{
			final ResponseEntity<Source> resEntity=restTemplate.exchange(searchURLs.getUserDocumentUrl(userId),HttpMethod.GET,
					reqEntity, Source.class);
			return Optional.ofNullable(resEntity.getBody());
		}catch(Exception e){
			LOG.error("ElasticSearch :=============== Error While Getting User Document============= " +e);
		}
		return Optional.empty();
	}
	

	
		
	
	
	public Boolean isDocumentExist(final Long userId) {

		final ElasticSearch es=ElasticSearch.withQuery(Query.withBool(Bool.withMust(Arrays.asList(
				Must.createMustWithMatch(Match.withUserId(userId))))));
		
		final HttpEntity<ElasticSearch> reqEntity = new HttpEntity<ElasticSearch>(es,getHttpHeaders());
		try {
			final ResponseEntity<String> resEntity=restTemplate.exchange(searchURLs.getExistDoucumentUrl(),HttpMethod.POST,	reqEntity, String.class);
			LOG.info("Found Document Response Code :"+resEntity.getStatusCode());
			return true; 
		} 
		catch (Exception e) {
			LOG.error("ElasticSearch :============== User Document does not Exist, UserId:============" + userId+e);
			return false;
		}
	}
	
	

	
	public void addDocument(final Source source) {

		final HttpEntity<Source> reqEntity = new HttpEntity<Source>(source,getHttpHeaders());
		
		try {
			final ResponseEntity<String> resEntity=restTemplate.exchange(searchURLs.getAddDoucumentUrl(source.getUserId())
					,HttpMethod.POST, reqEntity, String.class);
			
            LOG.info("Add Document [status : " + resEntity.getStatusCode()+ ", response : " + resEntity.getBody() + " ]");
       } catch (Exception e) {
			LOG.error("ElasticSearch :================== Error While Add Document, UserId=============== :"+ source.getUserId(), e);
		}
	}
	
	public Boolean deleteUserDocument(Long userId) {
		
		final HttpEntity<String> reqEntity = new HttpEntity<String>(getHttpHeaders());
		try{
			final ResponseEntity<Source> resEntity=restTemplate.exchange(searchURLs.getDeleteDoucumentUrl(userId),HttpMethod.DELETE,
					reqEntity, Source.class);
				
			if(resEntity.getStatusCode()==HttpStatus.OK){
				return true;
			}
		}catch(Exception e){
			LOG.error("ElasticSearch :=============== Error While Deleting User Document ==============" ,e);
		}
		return false;
	}
	
public Set<Long> getBlockedUserIds(Long userId) {
	 Set<Long> blockedUsers=null;
		
		final HttpEntity<String> reqEntity = new HttpEntity<String>(getHttpHeaders());
		try{
			final ResponseEntity<Source> resEntity=restTemplate.exchange(searchURLs.getUserDocumentUrl(userId),HttpMethod.GET,
					reqEntity, Source.class);
			return resEntity.getBody().getBlockedIds();
		}catch(Exception e){
			LOG.error("ElasticSearch :================= Error While Getting Blocked users=============== " +e);
		}
		return blockedUsers;
	}
	


public Set<Long> getRequestSentMentors(Long userId) {
	 Set<Long> blockedUsers=null;
		
		final HttpEntity<String> reqEntity = new HttpEntity<String>(getHttpHeaders());
		try{
			final ResponseEntity<Source> resEntity=restTemplate.exchange(searchURLs.getUserDocumentUrl(userId),HttpMethod.GET,
					reqEntity, Source.class);
			return resEntity.getBody().getRequestSentMentors();
		}catch(Exception e){
			LOG.error("ElasticSearch :================= Error While Getting Blocked users=============== " +e);
		}
		return blockedUsers;
}

   public Set<Long> getSkippedMentors(Long userId) {
	 Set<Long> blockedUsers=null;
		
		final HttpEntity<String> reqEntity = new HttpEntity<String>(getHttpHeaders());
		try{
			final ResponseEntity<Source> resEntity=restTemplate.exchange(searchURLs.getUserDocumentUrl(userId),HttpMethod.GET,
					reqEntity, Source.class);
			return resEntity.getBody().getSkippedMentors();
		}catch(Exception e){
			LOG.error("ElasticSearch :================= Error While Getting Blocked users=============== " +e);
		}
		return blockedUsers;
}


	
	public void updateDocumentWithScript(final Long userId, final Script script, final String tag)  {
		
		final Document doc=Document.withScript(script);
		final HttpEntity<Document> reqEntity = new HttpEntity<Document>(doc,getHttpHeaders());
		try {
			LOG.info("[ Json: "+new ObjectMapper().writeValueAsString(doc)+" ]");
			final ResponseEntity<String> resEntity = restTemplate.exchange(searchURLs.getUpdateDoucumentUrl(userId),
					HttpMethod.POST,reqEntity,String.class);
           LOG.info("Update "+tag+ "Json: "+new ObjectMapper().writeValueAsString(doc)+" [status : " + resEntity.getStatusCode()+ " ]");
        } catch (Exception e) {
			LOG.error("ElasticSearch :=============== Error While Update -Json: "+ tag +", UserId =============:"+ userId, e);
		}
	}
	
	
	
	
	private class ElasticSearchURLs {

		public static final String searchFrom="_search?from=";
		public static final String search="_search";
		public static final String size="&size=";
		public static final String update="/_update";
		public static final String exist="_search/exists";
		public static final String source="/_source";
		public static final String RETRY="?retry_on_conflict=5";
		
		public String getSearchUrl(final Integer offset, final Integer limit) {
			return new StringBuilder().append(getBaseUrlForFreemium()).append(searchFrom)
					.append(offset).append(size).append(limit).toString();
		}
		

		public String getUserProperityUrl() {
			return new StringBuilder().append(getBaseUrlForFreemium()).append(search).toString();
		}

		public String getUserDocumentUrl(Long userId) {
			return new StringBuilder().append(getBaseUrlForFreemium()).append(userId).append(source).toString();
		}

		public String getAddDoucumentUrl(final Long userId) {
			return new StringBuilder().append(getBaseUrlForFreemium()).append(userId).toString();
		}
		public String getDeleteDoucumentUrl(final Long userId) {
			return getAddDoucumentUrl(userId);
		}

		public String getUpdateDoucumentUrl(final Long userId) {
			return new StringBuilder().append(getBaseUrlForFreemium()).append(userId).append(update)
					.append(RETRY).toString();
		}

		public String getExistDoucumentUrl() {
			return new StringBuilder().append(getBaseUrlForFreemium()).append(exist).toString();
		}
		
		public String getBaseUrlForFreemium(){
			return new StringBuilder(baseUrl).append("/").append(Index.MENTORZ)
					.append("/").append(Index.Type.USERS).append("/").toString();
		}
		
    }







*/




	

}
