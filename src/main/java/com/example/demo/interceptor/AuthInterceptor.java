/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.example.demo.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.example.demo.annotation.Auth;




@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {
	 
	private final Logger _logger = LoggerFactory.getLogger(AuthInterceptor.class);
	@Context
    private ResourceInfo resourceInfo;
	
	
	/*private final AuthorizationService authorizationService;*/

	@Context
	private UriInfo uriInfo;
	
//	@Autowired
//	public AuthInterceptor(AuthorizationService authorizationService) {
//		this.authorizationService = authorizationService;
//	}
	
	@Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception)
    throws Exception {
  

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
    throws Exception {
    

    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
     
     String url=request.getRequestURI();
     String method1=  request.getMethod();
      String[] arr= url.split("userId/");
    
/*
   // Method method = resourceInfo.getResourceMethod();
      Class<Auth> obj = Auth.class;
      if (obj.isAnnotationPresent(Auth.class)) {
     {
    	 System.out.println("==========auth is present===========");
    	if(userId == null || !authorizationService.authorize(userId,
				request.getHeader(Handler.HEADER_PARAM_OAUTH_TOKEN),
				request.getHeader(Handler.HEADER_PARAM_USER_AGENT))) {
			_logger.info("Unauthorized for token " 
				+ request.getHeader(Handler.HEADER_PARAM_OAUTH_TOKEN)
				+ " UserAgent " + request.getHeader(Handler.HEADER_PARAM_USER_AGENT));
			
			
			  
			//request.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		//}
 
    	 return true;
    	 
     }
     
    }*/
     
	return true;
    }
    
    	 
    
   } 
     
