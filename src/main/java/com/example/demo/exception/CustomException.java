package com.example.demo.exception;

public class CustomException extends RuntimeException {
private static final long serialVersionUID = 1L;
	
	
     private final Integer status;
     public CustomException(final Integer status, final String msg) {
 		super(msg);
 		this.status = status;
 	}
 	
 	public CustomException(final Integer status, final String msg, final Throwable cause) {
 		super(msg,cause);
 		this.status = status;
 	}

 	
 	
 	public Integer getStatus() {
 		return status;
 	}
}
