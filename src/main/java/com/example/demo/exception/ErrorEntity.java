/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.example.demo.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Navrattan Yadav
 *
 */
public class ErrorEntity {

	private int errorCode;
	private String errorMsg;
	
	public ErrorEntity(int errorCode, String errorMsg) {
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}

	@SuppressWarnings("unused")
	private ErrorEntity() {
		this(0, null);
	}

	@JsonProperty("error_code")
	public int getErrorCode() {
		return errorCode;
	}

	@JsonProperty("error_msg")
	public String getErrorMsg() {
		return errorMsg;
	}

	
	
}
