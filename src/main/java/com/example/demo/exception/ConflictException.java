
package com.example.demo.exception;

import javax.ws.rs.core.Response.Status;

public class ConflictException extends CustomException {
	
	private static final long serialVersionUID = 1L;
	private final static Integer CONFLICT = Status.CONFLICT.getStatusCode();

	public ConflictException(String msg) {

		super(CONFLICT, "Alreay exists");

	}
}
