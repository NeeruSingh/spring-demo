package com.example.demo.controller;



import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.model.User;


@RequestMapping(path="/springboot")
public interface UserController {
	
	@RequestMapping(path="/api/user",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	ResponseEntity<Object> register(@RequestBody User user, @RequestHeader(value="User-Agent") 
	String userAgent);               //done
	
	//@ResponseBody
	@RequestMapping(path="api/user/userIdid/{userid}", method = RequestMethod.GET)
	ResponseEntity<Object> getById( @PathVariable(value="userid") Integer userid,@RequestHeader(value="User-Agent") 
	String userAgent,@RequestHeader( value="HEADER_PARAM_OAUTH_TOKEN") String oauthToken,
	HttpServletRequest request);            

	//@ResponseBody
	 @RequestMapping(path="/api/user/name/{name}", method = RequestMethod.GET)
	 ResponseEntity<User> getByName( @PathVariable(value="name") String name,
			 @RequestHeader(value="User-Agent")  String userAgent,
			 @RequestHeader( value="HEADER_PARAM_OAUTH_TOKEN") String oauthToken, 
			 HttpServletRequest request);
 }
