package com.example.demo.controller.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.annotation.Auth;
import com.example.demo.controller.UserController;
import com.example.demo.model.User;
import com.example.demo.service.impl.UserServiceImpl;
@Controller
public class UserControllerImpl implements UserController {

	
	@Autowired
    private UserServiceImpl userService;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserControllerImpl.class);
	
	
	@Override
	public ResponseEntity<Object> register(@RequestBody User user, @RequestHeader(value="User-Agent")  
	String userAgent) {
		userService.register(user);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}


	
	@Override
	@Auth
	public ResponseEntity<Object> getById(@PathVariable(value="userid") Integer userid,
			@RequestHeader(value="User-Agent")  String userAgent,
			@RequestHeader( value="oauth-token") String oauthToken,HttpServletRequest request) {
		
		 return new ResponseEntity<Object>(userService.getById(userid),HttpStatus.OK);
	    }
	
	
    @Override
    @Auth
    public ResponseEntity<User> getByName(@PathVariable(value="name") String name,
    		@RequestHeader(value="User-Agent")  String userAgent,
    		@RequestHeader( value="oauth-token") String oauthToken, HttpServletRequest request) {
    	
		return new ResponseEntity<User>(userService.getUserByName(name),HttpStatus.OK);
	}


}
