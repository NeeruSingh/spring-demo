/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.example.demo.model.auth;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Token {

	@NotBlank(message = "User ID can't be null")
	private String userId;

	@NotBlank(message = "User agent can't be null")
	private String userAgent;

	@NotBlank(message = "token can't be null")
	private String authToken;

	public Token() {
	}

	public Token(String userId, String userAgent, String authToken) {
		this.userId = userId;
		this.userAgent = userAgent;
		this.authToken = authToken;
	}

	@JsonProperty("user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@JsonProperty("user_agent")
	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	@JsonProperty("token")
	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	@Override
	public String toString() {
		return "Token [user_id=" + userId + ", user_agent=" + userAgent + ", auth_token=" + authToken + "]";
	}

}
