package com.example.demo.model;

public class User {

	
	private final  Long id;
	
	private final String email;
	private final String password;
	
	private final String name;
	
	
	
	
	@SuppressWarnings("unused")
	private User(){
		this(null,null,null,null);
	}
	
	public User(final  Long id,final String email,final String password,final String name){
		this.id=id;
		this.email=email;
		this.password=password;
		this.name=name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	
}
