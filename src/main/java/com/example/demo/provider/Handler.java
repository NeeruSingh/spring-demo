/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.example.demo.provider;


public interface Handler {

	public static final String HEADER_PARAM_USER_AGENT = "user-agent";
	public static final String HEADER_PARAM_OAUTH_TOKEN = "oauth-token";
}
