package com.example.demo.dao;

import com.example.demo.model.User;

public interface UserDao {
	
	public User getUserById(Integer id);
	public void register(User user);
	public User getUserByName(String name);

}
