package com.example.demo.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;



@Component
public class BaseDaoFactory {

	private final NamedParameterJdbcOperations jdbctemplate;

	private static final Logger LOGGER = Logger.getLogger(BaseDaoFactory.class.getName());
	
	@Autowired
	public BaseDaoFactory(@Qualifier("jdbctemplate") NamedParameterJdbcOperations jdbctemplate) {
		this.jdbctemplate = jdbctemplate;
	}

	public BaseDAOImpl create(String fileName) {
		Properties prop = new Properties();
		HashMap<String, String> queryMap = new HashMap<String, String>();
		try {
			InputStream in=this.getClass().getResourceAsStream("/db/" + fileName);
			prop.loadFromXML(in);
			// prop.keySet().forEach(key -> queryMap.put((String)key,
			// prop.getProperty((String)key)));
			final Iterator<Object> i = prop.keySet().iterator();
			while (i.hasNext()) {
				final String key = (String) i.next();
				queryMap.put(key, (String) prop.get(key));
			}

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, " IO EXception found "+e);
		}

		return new BaseDAOImpl( queryMap,jdbctemplate);
	}

}
