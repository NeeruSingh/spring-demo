package com.example.demo.dao.impl;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;


import com.example.demo.dao.BaseDAO;
import com.example.demo.dao.UserDao;
import com.example.demo.exception.ConflictException;
import com.example.demo.model.User;

@Repository
public class UserDaoImpl implements UserDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);
	private final BaseDAO baseDao;
	private final static String[] COLUMN_ID = { "id" };

	public User getUserById(Integer id) {
		return new User((long) 1,"sxdxd",null,null);	
	}

	@Autowired
	public UserDaoImpl(BaseDaoFactory factory) {
		this.baseDao = factory.create("user.xml");
	}
	
	public void register(User user) {
		final KeyHolder keyholder = new GeneratedKeyHolder();
		try {
			final String registeruserQuery = baseDao.getQueryById("register.user");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("id",user.getId()).
					addValue("name",user.getName())
					.addValue("email", user.getEmail()).
					addValue("password",user.getPassword())
					.addValue("email_verified",0).
					addValue("registered_on", Instant.now().toEpochMilli())
					.addValue("account_status", 0);
			
			//baseDao.getJdbcTemplate().update(registeruserQuery, source);
			baseDao.getJdbcTemplate().update(registeruserQuery, source, keyholder, COLUMN_ID);
			LOGGER.info("*********** successfully registered********** ");
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("**********unable to register*******");
			
		}
		
		catch (ConflictException e) {
			LOGGER.info("**********unable to register,user already exists*******");
			throw new ConflictException("User already exists");
		}
		
		catch (Exception e) {
			LOGGER.error("******** unable to register user *********"+ e);
		}
		
	}

	public User getUserByName(String name) {
		return new User((long) 2,"neeru",null,null);
	}

}
