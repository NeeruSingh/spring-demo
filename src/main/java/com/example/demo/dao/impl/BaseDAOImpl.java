/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.example.demo.dao.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.example.demo.dao.BaseDAO;




public class BaseDAOImpl implements BaseDAO {

	private final NamedParameterJdbcOperations jdbcTemplate;

	private final Map<String, String> queryMap;

	protected Logger logger;

	public BaseDAOImpl(final Map<String, String> queryMap, final NamedParameterJdbcOperations jdbcTemplate) {
		this.queryMap = queryMap;
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public NamedParameterJdbcOperations getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	public String getQueryById(final String id) {
		return queryMap.get(id);
	}

	@Override
	public int insert(String query, Map<String, Object> paramMap) {
		return jdbcTemplate.update(query, paramMap);
	}

	@Override
	public int getRowCount(String query, SqlParameterSource source) {
		try {
			return jdbcTemplate.queryForObject(query, source, Integer.class);
		} catch (EmptyResultDataAccessException e) {
			logger.info(e);
			return 0;
		}
	}

}
