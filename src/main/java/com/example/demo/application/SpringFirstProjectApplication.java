package com.example.demo.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mchange.v2.c3p0.ComboPooledDataSource;



@SpringBootApplication
@ComponentScan(basePackages = {
        "com.example.demo.controller",
        "com.example.demo.service",
        "com.example.demo.dao","com.example.demo.elasticsearch","com.example.demo.interceptor"  })
@ImportResource({"classpath:/spring/application-config.xml"})
   public class SpringFirstProjectApplication {
	private ClassPathXmlApplicationContext classPathXmlApplicationContext;

	public static void main(String[] args) {
		SpringApplication.run(SpringFirstProjectApplication.class, args);
	}
	

	public void start() throws Exception {
		// Auto-generated method stub

	}
	public void stop() throws Exception {
		final ComboPooledDataSource comboPooledDataSource = classPathXmlApplicationContext
				.getBean(ComboPooledDataSource.class);
		comboPooledDataSource.close();
		classPathXmlApplicationContext.close();
	}
}
