package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.example.demo.interceptor.AuthInterceptor;


@Configuration
public class AppConfig extends WebMvcConfigurerAdapter  {
	
	@Autowired
	AuthInterceptor requestInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		 registry.addInterceptor(new AuthInterceptor()).addPathPatterns("/springboot/api/user/id/{id}")
		 .addPathPatterns("/springboot/api/user/name/{name}");
	}
}
