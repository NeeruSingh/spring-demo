package com.example.demo.service;

import com.example.demo.model.User;

public interface UserService {

	void register(User user);
    Object getById(Integer id);
	Object getUserByName(String name);
}
