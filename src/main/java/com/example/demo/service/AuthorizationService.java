/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.example.demo.service;


public interface AuthorizationService {

	String getOrCreateOAuthToken(String userId, String userAgent);

	boolean authorize(String userId, String token, String userAgent);

	boolean deleteToken(String token, String userAgent);
}