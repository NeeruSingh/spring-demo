/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.example.demo.service.impl;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


import com.example.demo.model.auth.Token;
import com.example.demo.service.AuthorizationService;



public class AuthorizationServiceImpl implements AuthorizationService {	

	private final Logger _logger = LoggerFactory.getLogger(AuthorizationServiceImpl.class);
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationServiceImpl.class);
	public final String ROOT_URL;
	public final String secretId;
	public final String secret;
	
	public AuthorizationServiceImpl(final String oauthServerURL, final String secretId, final String secret) {
		this.ROOT_URL = oauthServerURL;
		this.secretId = secretId;
		this.secret = secret;
	}

	@Override
	public String getOrCreateOAuthToken(String userId, String userAgent) {
		/*try {*/
			
			RestTemplate restTemplate = new RestTemplate();
			Token tr = new Token();
			tr.setUserAgent(userAgent);
			_logger.info("userId= "+userId +" userAgent = "+userAgent);		
			tr.setUserId(userId);
			HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.set("secret", secret);
		    headers.set("secretId", secretId);
		    HttpEntity<Token> entity = new HttpEntity<Token>(tr, headers);
		    _logger.info("Root url ="+ROOT_URL);
		    _logger.info("entity = "+entity+"userId="+userId);
			ResponseEntity<Token> result = restTemplate.exchange(ROOT_URL,HttpMethod.PUT, entity, Token.class);
			Token token = result.getBody();
			LOGGER.info("USER-AGENT =  "+userAgent);
			return token.getAuthToken() + "_" + userId;
			
			
			/*} catch (HttpClientErrorException e) {
				if ( e.getStatusCode() ==  HttpStatus.LOCKED ) {
					_logger.info("locked user exception  ="+e);
					throw new LockedException(" locked exception "+e);
					} if ( e.getStatusCode() == HttpStatus.FORBIDDEN) {
						throw new ForbiddenException(" forbidden exception "+e);
						
						} else {
							throw new InternalServerError(" internal server error "+e);
							}
					} */
	}

	@Override
	public boolean authorize(String userId,String token, String userAgent) {
		try {
			if(token.lastIndexOf("_") <= 0 || token.length() <= token.lastIndexOf("_")) {
				_logger.error("======Token is unautherized========");
				return false;
			}
			String tokenWithoutUserId = token.substring(0, token.lastIndexOf('_'));
			//String userId = token.substring(token.lastIndexOf('_') + 1);
			
			RestTemplate restTemplate = new RestTemplate();
			Token t = new Token(userId,userAgent,tokenWithoutUserId);
			HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.set("secret", secret);
		    headers.set("secretId", secretId);
		    HttpEntity<Token> entity = new HttpEntity<Token>(t, headers);
		    ResponseEntity<String> response = restTemplate.exchange(ROOT_URL + "/authorize",HttpMethod.POST, entity, String.class);
			if(response.getStatusCode() == HttpStatus.OK || response.getStatusCode() == HttpStatus.NO_CONTENT) {
				
				return true;
			}
		} catch (Exception e) {
			_logger.error(token , e);
		}
		_logger.error("======token is not autherized==========" );
		return false;
	}

	@Override
	public boolean deleteToken(String token, String userAgent) {
		try {
			if(token.lastIndexOf("_") <= 0 || token.length() <= token.lastIndexOf("_")) {
				return false;
			}
			String tokenWithoutUserId = token.substring(0, token.lastIndexOf('_'));
			String userId = token.substring(token.lastIndexOf('_') + 1);
			
			RestTemplate restTemplate = new RestTemplate();
			Token t = new Token(userId,userAgent,tokenWithoutUserId);
			HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.set("secret", secret);
		    headers.set("secretId", secretId);
		    HttpEntity<Token> entity = new HttpEntity<Token>(t, headers);
		    ResponseEntity<String> response = restTemplate.exchange(ROOT_URL ,HttpMethod.DELETE, entity, String.class);
			if(response.getStatusCode() == HttpStatus.OK ) {
				return true;
			}
		} catch (Exception e) {
			_logger.error(token , e);
		}
		return false;

	}
}
