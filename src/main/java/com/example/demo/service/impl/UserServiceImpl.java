package com.example.demo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.impl.UserDaoImpl;
import com.example.demo.elasticsearch.ElasticSearchHttpClient;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
@Service
public class UserServiceImpl implements UserService{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDaoImpl userDao;
	
	@Autowired
	private ElasticSearchHttpClient esHttpClient;
	
	@Override
	public void register(User user) {
		userDao.register(user);
	}

	@Override
	public User getById(Integer id) {
		return userDao.getUserById(id);
		
	}

	public User getUserByName(String name) {
		return userDao.getUserByName(name);
	}

	

}
